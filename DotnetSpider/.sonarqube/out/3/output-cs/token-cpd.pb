�7
x/Users/rokt/Documents/Rokt/Security-Engineer/Projects/DotnetSpider/src/DotnetSpider.MySql/AgentCenter/MySqlAgentStore.cs
	namespace		 	
DotnetSpider		
 
.		 
MySql		 
.		 
AgentCenter		 (
{

 
public 
class 
MySqlAgentStore 
: 
IAgentStore  +
{ 
private 	
readonly
 
AgentCenterOptions %
_options& .
;. /
public 
MySqlAgentStore	 
( 
IOptions !
<! "
AgentCenterOptions" 4
>4 5
options6 =
)= >
{ 
_options 
= 
options 
. 
Value 
; 
} 
public 
async	 
Task .
"EnsureDatabaseAndTableCreatedAsync 6
(6 7
)7 8
{ 
using 
(	 

var
 
conn 
= 
new 
MySqlConnection (
(( )
_options) 1
.1 2
ConnectionString2 B
)B C
)C D
{ 
await 	
conn
 
. 
ExecuteAsync 
( 
$" (
CREATE SCHEMA IF NOT EXISTS  #
{# $
_options$ ,
., -
Database- 5
}5 6+
 DEFAULT CHARACTER SET utf8mb4;6 U
"U V
)V W
;W X
var 
sql1 
= 
$" '
create table if not exists  "
{" #
_options# +
.+ ,
Database, 4
}4 5�
�.agent (id varchar(36) primary key, `name` varchar(255) null, processor_count int null, total_memory int null, is_deleted tinyint(1) default 0 null, creation_time timestamp default CURRENT_TIMESTAMP not null, last_modification_time timestamp default CURRENT_TIMESTAMP not null, key NAME_INDEX (`name`));	5 �
"
� �
;
� �
var 
sql2 
= 
$" '
create table if not exists  "
{" #
_options# +
.+ ,
Database, 4
}4 5�
�.agent_heartbeat(id bigint AUTO_INCREMENT primary key, agent_id varchar(36) not null, `agent_name` varchar(255) null, cpu_load int, free_memory int null, creation_time timestamp default CURRENT_TIMESTAMP not null, key NAME_INDEX (`agent_name`), key ID_INDEX (`agent_id`));	5 �
"
� �
;
� �
await 	
conn
 
. 
ExecuteAsync 
( 
sql1  
)  !
;! "
await 	
conn
 
. 
ExecuteAsync 
( 
sql2  
)  !
;! "
}   
}!! 
public## 
async##	 
Task## 
<## 
IEnumerable## 
<##  
	AgentInfo##  )
>##) *
>##* +
GetAllListAsync##, ;
(##; <
)##< =
{$$ 
using%% 
(%%	 

var%%
 
conn%% 
=%% 
new%% 
MySqlConnection%% (
(%%( )
_options%%) 1
.%%1 2
ConnectionString%%2 B
)%%B C
)%%C D
{&& 
return'' 

('' 
await'' 
conn'' 
.'' 

QueryAsync'' !
<''! "
	AgentInfo''" +
>''+ ,
('', -
$"(( 
SELECT * FROM (( 
{(( 
_options(( 
.(( 
Database(( '
}((' (
.agent((( .
"((. /
)((/ 0
)((0 1
;((1 2
})) 
}** 
public,, 
async,,	 
Task,, 
RegisterAsync,, !
(,,! "
	AgentInfo,," +
agent,,, 1
),,1 2
{-- 
using.. 
(..	 

var..
 
conn.. 
=.. 
new.. 
MySqlConnection.. (
(..( )
_options..) 1
...1 2
ConnectionString..2 B
)..B C
)..C D
{// 
var00 
a00 	
=00
 
await00 
conn00 
.00 
ExecuteAsync00 #
(00# $
$"11 
INSERT IGNORE INTO 11 
{11 
_options11 #
.11# $
Database11$ ,
}11, -�
�.agent (id, `name`, processor_count, total_memory, creation_time, last_modification_time) VALUES (@Id, @Name, @ProcessorCount, @TotalMemory, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP); UPDATE 	11- �
{
11� �
_options
11� �
.
11� �
Database
11� �
}
11� �:
,.agent SET is_deleted = false WHERE id = @Id
11� �
"
11� �
,
11� �
new22 
{22	 

agent22
 
.22 
Id22 
,22 
agent22 
.22 
Name22 
,22 
agent22  %
.22% &
ProcessorCount22& 4
,224 5
agent226 ;
.22; <
TotalMemory22< G
}22G H
)22H I
;22I J
}33 
}44 
public66 
async66	 
Task66 
HeartbeatAsync66 "
(66" #
AgentHeartbeat66# 1
	heartbeat662 ;
)66; <
{77 
using88 
(88	 

var88
 
conn88 
=88 
new88 
MySqlConnection88 (
(88( )
_options88) 1
.881 2
ConnectionString882 B
)88B C
)88C D
{99 
await:: 	
conn::
 
.:: 
ExecuteAsync:: 
(:: 
$";; 
INSERT IGNORE INTO ;; 
{;; 
_options;; #
.;;# $
Database;;$ ,
};;, -�
�.agent_heartbeat (agent_id, agent_name, free_memory, cpu_load, creation_time) VALUES (@AgentId, @AgentName, @FreeMemory, @CpuLoad, CURRENT_TIMESTAMP);	;;- �
"
;;� �
,
;;� �
new<< 
{<<	 

	heartbeat<<
 
.<< 
AgentId<< 
,<< 
	heartbeat<< &
.<<& '
	AgentName<<' 0
,<<0 1
	heartbeat<<2 ;
.<<; <

FreeMemory<<< F
,<<F G
	heartbeat<<H Q
.<<Q R
CpuLoad<<R Y
}<<Y Z
)<<Z [
;<<[ \
await== 	
conn==
 
.== 
ExecuteAsync== 
(== 
$">> 
UPDATE >> 
{>> 
_options>> 
.>> 
Database>>  
}>>  !U
I.agent SET last_modification_time = CURRENT_TIMESTAMP WHERE id = @AgentId>>! j
">>j k
,>>k l
new?? 
{??	 

	heartbeat??
 
.?? 
AgentId?? 
,?? 
}?? 
)?? 
;?? 
}@@ 
}AA 
}BB 
}CC ��
}/Users/rokt/Documents/Rokt/Security-Engineer/Projects/DotnetSpider/src/DotnetSpider.MySql/AgentCenter/MySqlStatisticsStore.cs
	namespace

 	
DotnetSpider


 
.

 
MySql

 
.

 
AgentCenter

 (
{ 
public 
class  
MySqlStatisticsStore "
:# $
IStatisticsStore% 5
{ 
private 	
readonly
 
AgentCenterOptions %
_options& .
;. /
public  
MySqlStatisticsStore	 
( 
IOptions &
<& '
AgentCenterOptions' 9
>9 :
options; B
)B C
{ 
_options 
= 
options 
. 
Value 
; 
} 
public 
async	 
Task .
"EnsureDatabaseAndTableCreatedAsync 6
(6 7
)7 8
{ 
using 
(	 

var
 
conn 
= 
new 
MySqlConnection (
(( )
_options) 1
.1 2
ConnectionString2 B
)B C
)C D
{ 
await 	
conn
 
. 
ExecuteAsync 
( 
$" (
CREATE SCHEMA IF NOT EXISTS  #
{# $
_options$ ,
., -
Database- 5
}5 6+
 DEFAULT CHARACTER SET utf8mb4;6 U
"U V
)V W
;W X
var 
sql1 
= 
$" '
create table if not exists  "
{" #
_options# +
.+ ,
Database, 4
}4 5�
�.statistics(id varchar(36) primary key, `name` varchar(255), `start` timestamp null, `exit` timestamp null, total bigint default 0 not null, success bigint default 0 not null, failure bigint default 0 not null, creation_time timestamp default CURRENT_TIMESTAMP not null, last_modification_time timestamp default CURRENT_TIMESTAMP not null, key CREATION_TIME_INDEX (`creation_time`), key LAST_MODIFICATION_TIME_INDEX (`last_modification_time`));	5 �
"
� �
;
� �
var 
sql2 
= 
$" '
create table if not exists  "
{" #
_options# +
.+ ,
Database, 4
}4 5�
�.agent_statistics(id varchar(36) primary key, `name` varchar(255), success bigint default 0 not null, failure bigint default 0 not null, elapsed_milliseconds bigint default 0 not null, creation_time timestamp default CURRENT_TIMESTAMP not null, last_modification_time timestamp default CURRENT_TIMESTAMP not null, key CREATION_TIME_INDEX (`creation_time`), key LAST_MODIFICATION_TIME_INDEX (`last_modification_time`));	5 �
"
� �
;
� �
await 	
conn
 
. 
ExecuteAsync 
( 
sql1  
)  !
;! "
await   	
conn  
 
.   
ExecuteAsync   
(   
sql2    
)    !
;  ! "
}!! 
}"" 
public$$ 
async$$	 
Task$$ 
IncreaseTotalAsync$$ &
($$& '
string$$' -
id$$. 0
,$$0 1
long$$2 6
count$$7 <
)$$< =
{%% 
var&& 
sql&& 

=&& 
$"'' 
INSERT INTO '' 
{'' 
_options'' 
.'' 
Database'' $
}''$ %�
�.statistics (id, total) VALUES (@OwnerId, @Count) ON DUPLICATE key UPDATE total = total + @Count, last_modification_time = CURRENT_TIMESTAMP;	''% �
"
''� �
;
''� �
using(( 
(((	 

var((
 
conn(( 
=(( 
new(( 
MySqlConnection(( (
(((( )
_options(() 1
.((1 2
ConnectionString((2 B
)((B C
)((C D
{)) 
await** 	
conn**
 
.** 
ExecuteAsync** 
(** 
sql** 
,**  
new++ 
{++	 

OwnerId++
 
=++ 
id++ 
,++ 
Count++ 
=++ 
count++  %
}++% &
)++& '
;++' (
},, 
}-- 
public// 
async//	 
Task//  
IncreaseSuccessAsync// (
(//( )
string//) /
id//0 2
)//2 3
{00 
var11 
sql11 

=11 
$"22 
INSERT INTO 22 
{22 
_options22 
.22 
Database22 $
}22$ %�
}.statistics (id) VALUES (@OwnerId) ON DUPLICATE key UPDATE success = success + 1, last_modification_time = CURRENT_TIMESTAMP;	22% �
"
22� �
;
22� �
using33 
(33	 

var33
 
conn33 
=33 
new33 
MySqlConnection33 (
(33( )
_options33) 1
.331 2
ConnectionString332 B
)33B C
)33C D
{44 
await55 	
conn55
 
.55 
ExecuteAsync55 
(55 
sql55 
,55  
new66 
{66	 

OwnerId66
 
=66 
id66 
}66 
)66 
;66 
}77 
}88 
public:: 
async::	 
Task::  
IncreaseFailureAsync:: (
(::( )
string::) /
id::0 2
)::2 3
{;; 
var<< 
sql<< 

=<< 
$"== 
INSERT INTO == 
{== 
_options== 
.== 
Database== $
}==$ %�
}.statistics (id) VALUES (@OwnerId) ON DUPLICATE key UPDATE failure = failure + 1, last_modification_time = CURRENT_TIMESTAMP;	==% �
"
==� �
;
==� �
using>> 
(>>	 

var>>
 
conn>> 
=>> 
new>> 
MySqlConnection>> (
(>>( )
_options>>) 1
.>>1 2
ConnectionString>>2 B
)>>B C
)>>C D
{?? 
await@@ 	
conn@@
 
.@@ 
ExecuteAsync@@ 
(@@ 
sql@@ 
,@@  
newAA 
{AA	 

OwnerIdAA
 
=AA 
idAA 
}AA 
)AA 
;AA 
}BB 
}CC 
publicEE 
asyncEE	 
TaskEE 

StartAsyncEE 
(EE 
stringEE %
idEE& (
,EE( )
stringEE* 0
nameEE1 5
)EE5 6
{FF 
varGG 
sqlGG 

=GG 
$"HH 
INSERT INTO HH 
{HH 
_optionsHH 
.HH 
DatabaseHH $
}HH$ %�
�.statistics (id, `name`, start) VALUES (@OwnerId, @Name, @Start) ON DUPLICATE key UPDATE start = @Start, last_modification_time = CURRENT_TIMESTAMP;	HH% �
"
HH� �
;
HH� �
usingII 
(II	 

varII
 
connII 
=II 
newII 
MySqlConnectionII (
(II( )
_optionsII) 1
.II1 2
ConnectionStringII2 B
)IIB C
)IIC D
{JJ 
awaitKK 	
connKK
 
.KK 
ExecuteAsyncKK 
(KK 
sqlKK 
,KK  
newLL 
{LL	 

OwnerIdLL
 
=LL 
idLL 
,LL 
StartLL 
=LL 
DateTimeOffsetLL  .
.LL. /
NowLL/ 2
,LL2 3
NameLL4 8
=LL9 :
nameLL; ?
}LL? @
)LL@ A
;LLA B
}MM 
}NN 
publicPP 
asyncPP	 
TaskPP 
	ExitAsyncPP 
(PP 
stringPP $
idPP% '
)PP' (
{QQ 
varRR 
sqlRR 

=RR 
$"SS 
INSERT INTO SS 
{SS 
_optionsSS 
.SS 
DatabaseSS $
}SS$ %�
�.statistics (id, `exit`) VALUES (@OwnerId, @Exit) ON DUPLICATE key UPDATE `exit` = @Exit, last_modification_time = CURRENT_TIMESTAMP;	SS% �
"
SS� �
;
SS� �
usingTT 
(TT	 

varTT
 
connTT 
=TT 
newTT 
MySqlConnectionTT (
(TT( )
_optionsTT) 1
.TT1 2
ConnectionStringTT2 B
)TTB C
)TTC D
{UU 
awaitVV 	
connVV
 
.VV 
ExecuteAsyncVV 
(VV 
sqlVV 
,VV  
newWW 
{WW	 

OwnerIdWW
 
=WW 
idWW 
,WW 
ExitWW 
=WW 
DateTimeOffsetWW -
.WW- .
NowWW. 1
}WW1 2
)WW2 3
;WW3 4
}XX 
}YY 
public[[ 
async[[	 
Task[[ 
RegisterAgentAsync[[ &
([[& '
string[[' -
agentId[[. 5
,[[5 6
string[[7 =
	agentName[[> G
)[[G H
{\\ 
var]] 
sql]] 

=]] 
$"^^ 
INSERT INTO ^^ 
{^^ 
_options^^ 
.^^ 
Database^^ $
}^^$ %�
�.agent_statistics (`id`, `agent_name`, creation_time) VALUES (@AgentId, @AgentName, CURRENT_TIMESTAMP) ON DUPLICATE key UPDATE  agent_name = @AgentName, last_modification_time = CURRENT_TIMESTAMP;	^^% �
"
^^� �
;
^^� �
using__ 
(__	 

var__
 
conn__ 
=__ 
new__ 
MySqlConnection__ (
(__( )
_options__) 1
.__1 2
ConnectionString__2 B
)__B C
)__C D
{`` 
awaitaa 	
connaa
 
.aa 
ExecuteAsyncaa 
(aa 
sqlaa 
,aa  
newbb 
{bb	 

AgentIdbb
 
=bb 
agentIdbb 
,bb 
	AgentNamebb &
=bb' (
	agentNamebb) 2
}bb2 3
)bb3 4
;bb4 5
}cc 
}dd 
publicff 
asyncff	 
Taskff %
IncreaseAgentSuccessAsyncff -
(ff- .
stringff. 4
agentIdff5 <
,ff< =
intff> A
elapsedMillisecondsffB U
)ffU V
{gg 
varhh 
sqlhh 

=hh 
$"ii 
INSERT INTO ii 
{ii 
_optionsii 
.ii 
Databaseii $
}ii$ %�
�.agent_statistics (id, elapsed_milliseconds, creation_time) VALUES (@AgentId, @ElapsedMilliseconds, CURRENT_TIMESTAMP) ON DUPLICATE key UPDATE success = success + 1, elapsed_milliseconds = elapsed_milliseconds + @ElapsedMilliseconds, last_modification_time = CURRENT_TIMESTAMP;	ii% �
"
ii� �
;
ii� �
usingjj 
(jj	 

varjj
 
connjj 
=jj 
newjj 
MySqlConnectionjj (
(jj( )
_optionsjj) 1
.jj1 2
ConnectionStringjj2 B
)jjB C
)jjC D
{kk 
awaitll 	
connll
 
.ll 
ExecuteAsyncll 
(ll 
sqlll 
,ll  
newmm 
{mm	 

AgentIdmm
 
=mm 
agentIdmm 
,mm 
ElapsedMillisecondsmm 0
=mm1 2
elapsedMillisecondsmm3 F
}mmF G
)mmG H
;mmH I
}nn 
}oo 
publicqq 
asyncqq	 
Taskqq %
IncreaseAgentFailureAsyncqq -
(qq- .
stringqq. 4
agentIdqq5 <
,qq< =
intqq> A
elapsedMillisecondsqqB U
)qqU V
{rr 
varss 
sqlss 

=ss 
$"tt 
INSERT INTO tt 
{tt 
_optionstt 
.tt 
Databasett $
}tt$ %�
�.agent_statistics (id, elapsed_milliseconds, creation_time) VALUES (@AgentId, @ElapsedMilliseconds, CURRENT_TIMESTAMP) ON DUPLICATE key UPDATE failure = failure + 1, elapsed_milliseconds = elapsed_milliseconds + @ElapsedMilliseconds, last_modification_time = CURRENT_TIMESTAMP;	tt% �
"
tt� �
;
tt� �
usinguu 
(uu	 

varuu
 
connuu 
=uu 
newuu 
MySqlConnectionuu (
(uu( )
_optionsuu) 1
.uu1 2
ConnectionStringuu2 B
)uuB C
)uuC D
{vv 
awaitww 	
connww
 
.ww 
ExecuteAsyncww 
(ww 
sqlww 
,ww  
newxx 
{xx	 

AgentIdxx
 
=xx 
agentIdxx 
,xx 
ElapsedMillisecondsxx 0
=xx1 2
elapsedMillisecondsxx3 F
}xxF G
)xxG H
;xxH I
}yy 
}zz 
public|| 
async||	 
Task|| 
<|| 
PagedResult|| 
<||  
AgentStatistics||  /
>||/ 0
>||0 1*
PagedQueryAgentStatisticsAsync||2 P
(||P Q
string||Q W
agentId||X _
,||_ `
int||a d
page||e i
,||i j
int}} 
limit}} 
)}} 
{~~ 
if 
( 
page 
<= 
$num 
) 
{
�� 
page
�� 
=
��	 

$num
�� 
;
�� 
}
�� 
if
�� 
(
�� 
limit
�� 
<=
�� 
$num
�� 
)
�� 
{
�� 
limit
�� 	
=
��
 
$num
�� 
;
�� 
}
�� 
if
�� 
(
�� 
limit
�� 
>
�� 
$num
�� 
)
�� 
{
�� 
limit
�� 	
=
��
 
$num
�� 
;
�� 
}
�� 
using
�� 
(
��	 

var
��
 
conn
�� 
=
�� 
new
�� 
MySqlConnection
�� (
(
��( )
_options
��) 1
.
��1 2
ConnectionString
��2 B
)
��B C
)
��C D
{
�� 
var
�� 
start
�� 
=
�� 
limit
�� 
*
�� 
(
�� 
page
�� 
-
�� 
$num
��  !
)
��! "
;
��" #
var
�� 
where
�� 
=
�� 
string
�� 
.
��  
IsNullOrWhiteSpace
�� )
(
��) *
agentId
��* 1
)
��1 2
?
��3 4
$str
��5 7
:
��8 9
$str
��: R
;
��R S
var
�� 
count
�� 
=
�� 
await
�� 
conn
�� 
.
�� 
QuerySingleAsync
�� +
<
��+ ,
int
��, /
>
��/ 0
(
��0 1
$"
�� #
SELECT COUNT(*) FROM 
�� 
{
�� 
_options
�� %
.
��% &
Database
��& .
}
��. / 
.agent_statistics 
��/ A
{
��A B
where
��B G
}
��G H
"
��H I
)
��I J
;
��J K
var
�� 
result
�� 
=
�� 
(
�� 
await
�� 
conn
�� 
.
�� 

QueryAsync
�� '
<
��' (
AgentStatistics
��( 7
>
��7 8
(
��8 9
$"
�� 
SELECT * FROM 
�� 
{
�� 
_options
�� 
.
�� 
Database
�� '
}
��' ( 
.agent_statistics 
��( :
{
��: ;
where
��; @
}
��@ A<
. ORDER BY creation_time LIMIT @Start, @Offset;
��A o
"
��o p
,
��p q
new
�� 
{
��	 

Start
��
 
=
�� 
start
�� 
,
�� 
Offfset
��  
=
��! "
limit
��# (
,
��( )
AgentId
��* 1
=
��2 3
agentId
��4 ;
}
��; <
)
��< =
)
��= >
;
��> ?
return
�� 

new
�� 
PagedResult
�� 
<
�� 
AgentStatistics
�� *
>
��* +
(
��+ ,
page
��, 0
,
��0 1
limit
��2 7
,
��7 8
count
��9 >
,
��> ?
result
��@ F
)
��F G
;
��G H
}
�� 
}
�� 
public
�� 
async
��	 
Task
�� 
<
�� 
AgentStatistics
�� #
>
��# $%
GetAgentStatisticsAsync
��% <
(
��< =
string
��= C
agentId
��D K
)
��K L
{
�� 
using
�� 
(
��	 

var
��
 
conn
�� 
=
�� 
new
�� 
MySqlConnection
�� (
(
��( )
_options
��) 1
.
��1 2
ConnectionString
��2 B
)
��B C
)
��C D
{
�� 
return
�� 

await
�� 
conn
�� 
.
�� '
QuerySingleOrDefaultAsync
�� /
<
��/ 0
AgentStatistics
��0 ?
>
��? @
(
��@ A
$"
�� 
SELECT * FROM 
�� 
{
�� 
_options
�� 
.
�� 
Database
�� '
}
��' (<
..agent_statistics WHERE id = @AgentId LIMIT 1;
��( V
"
��V W
,
��W X
new
�� 
{
��	 

AgentId
��
 
=
�� 
agentId
�� 
}
�� 
)
�� 
;
�� 
}
�� 
}
�� 
public
�� 
async
��	 
Task
�� 
<
�� 
SpiderStatistics
�� $
>
��$ %&
GetSpiderStatisticsAsync
��& >
(
��> ?
string
��? E
id
��F H
)
��H I
{
�� 
using
�� 
(
��	 

var
��
 
conn
�� 
=
�� 
new
�� 
MySqlConnection
�� (
(
��( )
_options
��) 1
.
��1 2
ConnectionString
��2 B
)
��B C
)
��C D
{
�� 
return
�� 

await
�� 
conn
�� 
.
�� '
QuerySingleOrDefaultAsync
�� /
<
��/ 0
SpiderStatistics
��0 @
>
��@ A
(
��A B
$"
�� 
SELECT * FROM 
�� 
{
�� 
_options
�� 
.
�� 
Database
�� '
}
��' (<
..statistics WHERE owner_id = @OwnerId LIMIT 1;
��( V
"
��V W
,
��W X
new
�� 
{
��	 

OwnerId
��
 
=
�� 
id
�� 
}
�� 
)
�� 
;
�� 
}
�� 
}
�� 
public
�� 
async
��	 
Task
�� 
<
�� 
PagedResult
�� 
<
��  
SpiderStatistics
��  0
>
��0 1
>
��1 2-
PagedQuerySpiderStatisticsAsync
��3 R
(
��R S
string
��S Y
keyword
��Z a
,
��a b
int
��c f
page
��g k
,
��k l
int
�� 
limit
�� 
)
�� 
{
�� 
if
�� 
(
�� 
page
�� 
<=
�� 
$num
�� 
)
�� 
{
�� 
page
�� 
=
��	 

$num
�� 
;
�� 
}
�� 
if
�� 
(
�� 
limit
�� 
<=
�� 
$num
�� 
)
�� 
{
�� 
limit
�� 	
=
��
 
$num
�� 
;
�� 
}
�� 
if
�� 
(
�� 
limit
�� 
>
�� 
$num
�� 
)
�� 
{
�� 
limit
�� 	
=
��
 
$num
�� 
;
�� 
}
�� 
using
�� 
(
��	 

var
��
 
conn
�� 
=
�� 
new
�� 
MySqlConnection
�� (
(
��( )
_options
��) 1
.
��1 2
ConnectionString
��2 B
)
��B C
)
��C D
{
�� 
var
�� 
start
�� 
=
�� 
limit
�� 
*
�� 
(
�� 
page
�� 
-
�� 
$num
��  !
)
��! "
;
��" #
var
�� 
where
�� 
=
�� 
string
�� 
.
��  
IsNullOrWhiteSpace
�� )
(
��) *
keyword
��* 1
)
��1 2
?
��3 4
$str
��5 7
:
��8 9
$str
��: W
;
��W X
var
�� 
count
�� 
=
�� 
await
�� 
conn
�� 
.
�� 
QuerySingleAsync
�� +
<
��+ ,
int
��, /
>
��/ 0
(
��0 1
$"
�� #
SELECT COUNT(*) FROM 
�� 
{
�� 
_options
�� %
.
��% &
Database
��& .
}
��. /
.statistics 
��/ ;
{
��; <
where
��< A
}
��A B
"
��B C
)
��C D
;
��D E
var
�� 
result
�� 
=
�� 
(
�� 
await
�� 
conn
�� 
.
�� 

QueryAsync
�� '
<
��' (
SpiderStatistics
��( 8
>
��8 9
(
��9 :
$"
�� 
SELECT * FROM 
�� 
{
�� 
_options
�� 
.
�� 
Database
�� '
}
��' (
.statistics 
��( 4
{
��4 5
where
��5 :
}
��: ;<
. ORDER BY creation_time LIMIT @Start, @Offset;
��; i
"
��i j
,
��j k
new
�� 
{
��	 

Start
��
 
=
�� 
start
�� 
,
�� 
Offfset
��  
=
��! "
limit
��# (
,
��( )
Keyword
��* 1
=
��2 3
$"
��4 6
%
��6 7
{
��7 8
keyword
��8 ?
}
��? @
%
��@ A
"
��A B
}
��B C
)
��C D
)
��D E
;
��E F
return
�� 

new
�� 
PagedResult
�� 
<
�� 
SpiderStatistics
�� +
>
��+ ,
(
��, -
page
��- 1
,
��1 2
limit
��3 8
,
��8 9
count
��: ?
,
��? @
result
��A G
)
��G H
;
��H I
}
�� 
}
�� 
}
�� 
}�� ��
o/Users/rokt/Documents/Rokt/Security-Engineer/Projects/DotnetSpider/src/DotnetSpider.MySql/MySqlEntityStorage.cs
	namespace 	
DotnetSpider
 
. 
MySql 
{ 
public 
class 
MySqlOptions 
{ 
private 	
readonly
 
IConfiguration !
_configuration" 0
;0 1
public 
MySqlOptions	 
( 
IConfiguration $
configuration% 2
)2 3
{ 
_configuration 
= 
configuration !
;! "
} 
public 
StorageMode	 
Mode 
=> 
string #
.# $
IsNullOrWhiteSpace$ 6
(6 7
_configuration7 E
[E F
$strF R
]R S
)S T
? 
StorageMode 
. 
Insert 
: 
( 
StorageMode 
) 
Enum 
. 
Parse 
( 
typeof #
(# $
StorageMode$ /
)/ 0
,0 1
_configuration2 @
[@ A
$strA M
]M N
)N O
;O P
public 
string	 
ConnectionString  
=>! #
_configuration$ 2
[2 3
$str3 K
]K L
;L M
} 
public   
class   
MySqlEntityStorage    
:  ! "/
#RelationalDatabaseEntityStorageBase  # F
{!! 
	protected"" 
virtual"" 
string"" 
Escape"" !
=>""" $
$str""% (
;""( )
public$$ 
static$$	 
	IDataFlow$$ 
CreateFromOptions$$ +
($$+ ,
IConfiguration$$, :
configuration$$; H
)$$H I
{%% 
var&& 
options&& 
=&& 
new&& 
MySqlOptions&& !
(&&! "
configuration&&" /
)&&/ 0
;&&0 1
return'' 	
new''
 
MySqlEntityStorage''  
(''  !
options''! (
.''( )
Mode'') -
,''- .
options''/ 6
.''6 7
ConnectionString''7 G
)''G H
;''H I
}(( 
public// 
MySqlEntityStorage//	 
(// 
StorageMode// '
mode//( ,
,//, -
string00 	
connectionString00
 
)00 
:00 
base00 "
(00" #
mode00# '
,00' (
connectionString11 
)11 
{22 
}33 
	protected:: 
override:: 
IDbConnection:: "
CreateDbConnection::# 5
(::5 6
string::6 <
connectString::= J
)::J K
{;; 
return<< 	
new<<
 
MySqlConnection<< 
(<< 
connectString<< +
)<<+ ,
;<<, -
}== 
	protectedDD 
overrideDD 
SqlStatementsDD "!
GenerateSqlStatementsDD# 8
(DD8 9
TableMetadataDD9 F
tableMetadataDDG T
)DDT U
{EE 
varFF 
sqlStatementsFF 
=FF 
newFF 
SqlStatementsFF (
{GG 
	InsertSqlHH 
=HH 
GenerateInsertSqlHH !
(HH! "
tableMetadataHH" /
,HH/ 0
falseHH1 6
)HH6 7
,HH7 8$
InsertIgnoreDuplicateSqlII 
=II 
GenerateInsertSqlII 0
(II0 1
tableMetadataII1 >
,II> ?
trueII@ D
)IID E
,IIE F
InsertAndUpdateSqlJJ 
=JJ &
GenerateInsertAndUpdateSqlJJ 3
(JJ3 4
tableMetadataJJ4 A
)JJA B
,JJB C
	UpdateSqlKK 
=KK 
GenerateUpdateSqlKK !
(KK! "
tableMetadataKK" /
)KK/ 0
,KK0 1
CreateTableSqlLL 
=LL "
GenerateCreateTableSqlLL +
(LL+ ,
tableMetadataLL, 9
)LL9 :
,LL: ;
CreateDatabaseSqlMM 
=MM %
GenerateCreateDatabaseSqlMM 1
(MM1 2
tableMetadataMM2 ?
)MM? @
,MM@ A
DatabaseSqlNN 
=NN 
stringNN 
.NN 
IsNullOrWhiteSpaceNN +
(NN+ ,
tableMetadataNN, 9
.NN9 :
SchemaNN: @
.NN@ A
DatabaseNNA I
)NNI J
?OO 
$strOO 	
:PP 
$"PP 	
{PP	 

EscapePP
 
}PP 
{PP 

GetNameSqlPP 
(PP 
tableMetadataPP *
.PP* +
SchemaPP+ 1
.PP1 2
DatabasePP2 :
)PP: ;
}PP; <
{PP< =
EscapePP= C
}PPC D
"PPD E
}QQ 
;QQ 
returnRR 	
sqlStatementsRR
 
;RR 
}SS 
	protectedZZ 
virtualZZ 
stringZZ %
GenerateCreateDatabaseSqlZZ 4
(ZZ4 5
TableMetadataZZ5 B
tableMetadataZZC P
)ZZP Q
{[[ 
return\\ 	
string\\
 
.\\ 
IsNullOrWhiteSpace\\ #
(\\# $
tableMetadata\\$ 1
.\\1 2
Schema\\2 8
.\\8 9
Database\\9 A
)\\A B
?]] 
$str]] 
:^^ 
$"^^ (
CREATE SCHEMA IF NOT EXISTS ^^ $
{^^$ %
Escape^^% +
}^^+ ,
{^^, -

GetNameSql^^- 7
(^^7 8
tableMetadata^^8 E
.^^E F
Schema^^F L
.^^L M
Database^^M U
)^^U V
}^^V W
{^^W X
Escape^^X ^
}^^^ _+
 DEFAULT CHARACTER SET utf8mb4;^^_ ~
"^^~ 
;	^^ �
}__ 
	protectedff 
virtualff 
stringff "
GenerateCreateTableSqlff 1
(ff1 2
TableMetadataff2 ?
tableMetadataff@ M
)ffM N
{gg 
varhh "
isAutoIncrementPrimaryhh 
=hh 
tableMetadatahh  -
.hh- ."
IsAutoIncrementPrimaryhh. D
;hhD E
varjj 
tableSqljj 
=jj 
GenerateTableSqljj "
(jj" #
tableMetadatajj# 0
)jj0 1
;jj1 2
varll 
builderll 
=ll 
newll 
StringBuilderll "
(ll" #
$"ll# %'
CREATE TABLE IF NOT EXISTS ll% @
{ll@ A
tableSqlllA I
}llI J
 (llJ L
"llL M
)llM N
;llN O
foreachnn 

(nn 
varnn 
columnnn 
innn 
tableMetadatann '
.nn' (
Columnsnn( /
)nn/ 0
{oo 
varpp 
	isPrimarypp 
=pp 
tableMetadatapp !
.pp! "
	IsPrimarypp" +
(pp+ ,
columnpp, 2
.pp2 3
Keypp3 6
)pp6 7
;pp7 8
varrr 
	columnSqlrr 
=rr 
GenerateColumnSqlrr %
(rr% &
columnrr& ,
.rr, -
Valuerr- 2
,rr2 3
	isPrimaryrr4 =
)rr= >
;rr> ?
iftt 
(tt 
	isPrimarytt 
)tt 
{uu 
buildervv 
.vv 
Appendvv 
(vv "
isAutoIncrementPrimaryvv *
?ww 
$"ww 

{ww
 
	columnSqlww 
}ww )
 AUTO_INCREMENT PRIMARY KEY, ww 2
"ww2 3
:xx 
$"xx 

{xx
 
	columnSqlxx 
}xx 
{xx 
(xx 
tableMetadataxx %
.xx% &
Primaryxx& -
.xx- .
Countxx. 3
>xx4 5
$numxx6 7
?xx8 9
$strxx: <
:xx= >
$strxx? L
)xxL M
}xxM N
, xxN P
"xxP Q
)xxQ R
;xxR S
}yy 
elsezz 
{{{ 
builder|| 
.|| 
Append|| 
(|| 
$"|| 
{|| 
	columnSql||  
}||  !
, ||! #
"||# $
)||$ %
;||% &
}}} 
}~~ 
builder
�� 

.
��
 
Remove
�� 
(
�� 
builder
�� 
.
�� 
Length
��  
-
��! "
$num
��# $
,
��$ %
$num
��& '
)
��' (
;
��( )
if
�� 
(
�� 
tableMetadata
�� 
.
�� 
Primary
�� 
!=
�� 
null
��  $
&&
��% '
tableMetadata
��( 5
.
��5 6
Primary
��6 =
.
��= >
Count
��> C
>
��D E
$num
��F G
)
��G H
{
�� 
builder
�� 
.
�� 
Append
�� 
(
�� 
$"
�� 
, PRIMARY KEY (
�� 
{
�� 
string
�� 
.
�� 
Join
�� "
(
��" #
$str
��# '
,
��' (
tableMetadata
��) 6
.
��6 7
Primary
��7 >
.
��> ?
Select
��? E
(
��E F
c
��F G
=>
��H J
$"
��K M
{
��M N
Escape
��N T
}
��T U
{
��U V

GetNameSql
��V `
(
��` a
c
��a b
)
��b c
}
��c d
{
��d e
Escape
��e k
}
��k l
"
��l m
)
��m n
)
��n o
}
��o p
)
��p q
"
��q r
)
��r s
;
��s t
}
�� 
if
�� 
(
�� 
tableMetadata
�� 
.
�� 
Indexes
�� 
.
�� 
Count
�� "
>
��# $
$num
��% &
)
��& '
{
�� 
foreach
�� 
(
�� 
var
�� 
index
�� 
in
�� 
tableMetadata
�� '
.
��' (
Indexes
��( /
)
��/ 0
{
�� 
var
�� 
name
��	 
=
�� 
index
�� 
.
�� 
Name
�� 
;
�� 
var
�� 
columnNames
��	 
=
�� 
string
�� 
.
�� 
Join
�� "
(
��" #
$str
��# '
,
��' (
index
��) .
.
��. /
Columns
��/ 6
.
��6 7
Select
��7 =
(
��= >
c
��> ?
=>
��@ B
$"
��C E
{
��E F
Escape
��F L
}
��L M
{
��M N

GetNameSql
��N X
(
��X Y
c
��Y Z
)
��Z [
}
��[ \
{
��\ ]
Escape
��] c
}
��c d
"
��d e
)
��e f
)
��f g
;
��g h
builder
�� 
.
�� 
Append
�� 
(
�� 
$"
�� 
, 
�� 
{
�� 
(
�� 
index
�� 
.
��  
IsUnique
��  (
?
��) *
$str
��+ 3
:
��4 5
$str
��6 8
)
��8 9
}
��9 :
 KEY 
��: ?
{
��? @
Escape
��@ F
}
��F G
{
��G H
name
��H L
}
��L M
{
��M N
Escape
��N T
}
��T U
 (
��U W
{
��W X
columnNames
��X c
}
��c d
)
��d e
"
��e f
)
��f g
;
��g h
}
�� 
}
�� 
builder
�� 

.
��
 
Append
�� 
(
�� 
$str
�� 
)
�� 
;
�� 
var
�� 
sql
�� 

=
�� 
builder
�� 
.
�� 
ToString
�� 
(
�� 
)
�� 
;
��  
return
�� 	
sql
��
 
;
�� 
}
�� 
	protected
�� 
virtual
�� 
string
�� 
GenerateInsertSql
�� ,
(
��, -
TableMetadata
��- :
tableMetadata
��; H
,
��H I
bool
��J N
ignoreDuplicate
��O ^
)
��^ _
{
�� 
var
�� 
columns
�� 
=
�� 
tableMetadata
�� 
.
�� 
Columns
�� &
;
��& '
var
�� $
isAutoIncrementPrimary
�� 
=
�� 
tableMetadata
��  -
.
��- .$
IsAutoIncrementPrimary
��. D
;
��D E
var
�� 
insertColumns
�� 
=
�� 
(
�� $
isAutoIncrementPrimary
�� 
?
�� 
columns
�� %
.
��% &
Where
��& +
(
��+ ,
c1
��, .
=>
��/ 1
c1
��2 4
.
��4 5
Key
��5 8
!=
��9 ;
tableMetadata
��< I
.
��I J
Primary
��J Q
.
��Q R
First
��R W
(
��W X
)
��X Y
)
��Y Z
:
��[ \
columns
��] d
)
��d e
.
�� 
ToArray
�� 
(
�� 
)
�� 
;
�� 
var
�� 

columnsSql
�� 
=
�� 
string
�� 
.
�� 
Join
�� 
(
��  
$str
��  $
,
��$ %
insertColumns
��& 3
.
��3 4
Select
��4 :
(
��: ;
c
��; <
=>
��= ?
$"
��@ B
{
��B C
Escape
��C I
}
��I J
{
��J K

GetNameSql
��K U
(
��U V
c
��V W
.
��W X
Key
��X [
)
��[ \
}
��\ ]
{
��] ^
Escape
��^ d
}
��d e
"
��e f
)
��f g
)
��g h
;
��h i
var
�� 
columnsParamsSql
�� 
=
�� 
string
��  
.
��  !
Join
��! %
(
��% &
$str
��& *
,
��* +
insertColumns
��, 9
.
��9 :
Select
��: @
(
��@ A
p
��A B
=>
��C E
$"
��F H
@
��H I
{
��I J
p
��J K
.
��K L
Key
��L O
}
��O P
"
��P Q
)
��Q R
)
��R S
;
��S T
var
�� 
tableSql
�� 
=
�� 
GenerateTableSql
�� "
(
��" #
tableMetadata
��# 0
)
��0 1
;
��1 2
var
�� 
sql
�� 

=
�� 
$"
�� 
INSERT 
�� 
{
�� 
(
�� 
ignoreDuplicate
�� 
?
��  
$str
��! )
:
��* +
$str
��, .
)
��. /
}
��/ 0
 INTO 
��0 6
{
��6 7
tableSql
��7 ?
}
��? @
 (
��@ B
{
��B C

columnsSql
��C M
}
��M N

) VALUES (
��N X
{
��X Y
columnsParamsSql
��Y i
}
��i j
);
��j l
"
��l m
;
��m n
return
�� 	
sql
��
 
;
�� 
}
�� 
	protected
�� 
virtual
�� 
string
�� 
GenerateUpdateSql
�� ,
(
��, -
TableMetadata
��- :
tableMetadata
��; H
)
��H I
{
�� 
if
�� 
(
�� 
tableMetadata
�� 
.
�� 
Updates
�� 
==
�� 
null
��  $
||
��% '
tableMetadata
��( 5
.
��5 6
Updates
��6 =
.
��= >
Count
��> C
==
��D F
$num
��G H
)
��H I
{
�� 
Logger
�� 

?
��
 
.
�� 

LogWarning
�� 
(
�� 
$str
�� 1
)
��1 2
;
��2 3
return
�� 

null
�� 
;
�� 
}
�� 
var
�� 
where
�� 
=
�� 
$str
�� 
;
�� 
foreach
�� 

(
�� 
var
�� 
column
�� 
in
�� 
tableMetadata
�� '
.
��' (
Primary
��( /
)
��/ 0
{
�� 
where
�� 	
+=
��
 
$"
�� 
{
�� 
Escape
�� 
}
�� 
{
�� 

GetNameSql
�� #
(
��# $
column
��$ *
)
��* +
}
��+ ,
{
��, -
Escape
��- 3
}
��3 4
 = @
��4 8
{
��8 9
column
��9 ?
}
��? @
 AND
��@ D
"
��D E
;
��E F
}
�� 
where
�� 
=
��	 

where
�� 
.
�� 
	Substring
�� 
(
�� 
$num
�� 
,
�� 
where
�� #
.
��# $
Length
��$ *
-
��+ ,
$num
��- .
)
��. /
;
��/ 0
var
�� 
setCols
�� 
=
�� 
string
�� 
.
�� 
Join
�� 
(
�� 
$str
�� !
,
��! "
tableMetadata
�� 
.
�� 
Updates
�� 
.
�� 
Select
��  
(
��  !
c
��! "
=>
��# %
$"
��& (
{
��( )
Escape
��) /
}
��/ 0
{
��0 1

GetNameSql
��1 ;
(
��; <
c
��< =
)
��= >
}
��> ?
{
��? @
Escape
��@ F
}
��F G
= @
��G J
{
��J K
c
��K L
}
��L M
"
��M N
)
��N O
)
��O P
;
��P Q
var
�� 
tableSql
�� 
=
�� 
GenerateTableSql
�� "
(
��" #
tableMetadata
��# 0
)
��0 1
;
��1 2
var
�� 
sql
�� 

=
�� 
$"
�� 
UPDATE 
�� 
{
�� 
tableSql
�� 
}
��  
 SET 
��  %
{
��% &
setCols
��& -
}
��- .
 WHERE 
��. 5
{
��5 6
where
��6 ;
}
��; <
;
��< =
"
��= >
;
��> ?
return
�� 	
sql
��
 
;
�� 
}
�� 
	protected
�� 
virtual
�� 
string
�� (
GenerateInsertAndUpdateSql
�� 5
(
��5 6
TableMetadata
��6 C
tableMetadata
��D Q
)
��Q R
{
�� 
if
�� 
(
�� 
!
�� 
tableMetadata
�� 
.
�� 

HasPrimary
��  
)
��  !
{
�� 
Logger
�� 

?
��
 
.
�� 

LogWarning
�� 
(
�� 
$str
�� :
)
��: ;
;
��; <
return
�� 

null
�� 
;
�� 
}
�� 
var
�� 
columns
�� 
=
�� 
tableMetadata
�� 
.
�� 
Columns
�� &
;
��& '
var
�� $
isAutoIncrementPrimary
�� 
=
�� 
tableMetadata
��  -
.
��- .$
IsAutoIncrementPrimary
��. D
;
��D E
var
�� 
insertColumns
�� 
=
�� 
(
�� $
isAutoIncrementPrimary
�� 
?
�� 
columns
�� %
.
��% &
Where
��& +
(
��+ ,
c1
��, .
=>
��/ 1
c1
��2 4
.
��4 5
Key
��5 8
!=
��9 ;
tableMetadata
��< I
.
��I J
Primary
��J Q
.
��Q R
First
��R W
(
��W X
)
��X Y
)
��Y Z
:
��[ \
columns
��] d
)
��d e
.
�� 
ToArray
�� 
(
�� 
)
�� 
;
�� 
var
�� 

columnsSql
�� 
=
�� 
string
�� 
.
�� 
Join
�� 
(
��  
$str
��  $
,
��$ %
insertColumns
��& 3
.
��3 4
Select
��4 :
(
��: ;
c
��; <
=>
��= ?
$"
��@ B
{
��B C
Escape
��C I
}
��I J
{
��J K

GetNameSql
��K U
(
��U V
c
��V W
.
��W X
Key
��X [
)
��[ \
}
��\ ]
{
��] ^
Escape
��^ d
}
��d e
"
��e f
)
��f g
)
��g h
;
��h i
var
�� 
columnsParamsSql
�� 
=
�� 
string
��  
.
��  !
Join
��! %
(
��% &
$str
��& *
,
��* +
insertColumns
��, 9
.
��9 :
Select
��: @
(
��@ A
p
��A B
=>
��C E
$"
��F H
@
��H I
{
��I J
p
��J K
.
��K L
Key
��L O
}
��O P
"
��P Q
)
��Q R
)
��R S
;
��S T
var
�� 
tableSql
�� 
=
�� 
GenerateTableSql
�� "
(
��" #
tableMetadata
��# 0
)
��0 1
;
��1 2
var
�� 
setCols
�� 
=
�� 
string
�� 
.
�� 
Join
�� 
(
�� 
$str
�� !
,
��! "
insertColumns
�� 
.
�� 
Select
�� 
(
�� 
c
�� 
=>
�� 
$"
��  
{
��  !
Escape
��! '
}
��' (
{
��( )

GetNameSql
��) 3
(
��3 4
c
��4 5
.
��5 6
Key
��6 9
)
��9 :
}
��: ;
{
��; <
Escape
��< B
}
��B C
= @
��C F
{
��F G
c
��G H
.
��H I
Key
��I L
}
��L M
"
��M N
)
��N O
)
��O P
;
��P Q
var
�� 
sql
�� 

=
�� 
$"
�� 
INSERT INTO 
�� 
{
�� 
tableSql
�� 
}
�� 
 (
�� 
{
�� 

columnsSql
�� )
}
��) *

) VALUES (
��* 4
{
��4 5
columnsParamsSql
��5 E
}
��E F(
) ON DUPLICATE key UPDATE 
��F `
{
��` a
setCols
��a h
}
��h i
;
��i j
"
��j k
;
��k l
return
�� 	
sql
��
 
;
�� 
}
�� 
	protected
�� 
virtual
�� 
string
�� 
GenerateTableSql
�� +
(
��+ ,
TableMetadata
��, 9
tableMetadata
��: G
)
��G H
{
�� 
var
�� 
	tableName
�� 
=
�� 

GetNameSql
�� 
(
�� 
GetTableName
�� *
(
��* +
tableMetadata
��+ 8
)
��8 9
)
��9 :
;
��: ;
var
�� 
database
�� 
=
�� 

GetNameSql
�� 
(
�� 
tableMetadata
�� *
.
��* +
Schema
��+ 1
.
��1 2
Database
��2 :
)
��: ;
;
��; <
return
�� 	
string
��
 
.
��  
IsNullOrWhiteSpace
�� #
(
��# $
database
��$ ,
)
��, -
?
�� 
$"
�� 
{
�� 	
Escape
��	 
}
�� 
{
�� 
	tableName
�� 
}
�� 
{
�� 
Escape
�� "
}
��" #
"
��# $
:
�� 
$"
�� 
{
�� 	
Escape
��	 
}
�� 
{
�� 
database
�� 
}
�� 
{
�� 
Escape
�� !
}
��! "
.
��" #
{
��# $
Escape
��$ *
}
��* +
{
��+ ,
	tableName
��, 5
}
��5 6
{
��6 7
Escape
��7 =
}
��= >
"
��> ?
;
��? @
}
�� 
	protected
�� 
virtual
�� 
string
�� 
GenerateColumnSql
�� ,
(
��, -
Column
��- 3
column
��4 :
,
��: ;
bool
��< @
	isPrimary
��A J
)
��J K
{
�� 
var
�� 

columnName
�� 
=
�� 

GetNameSql
�� 
(
�� 
column
�� %
.
��% &
Name
��& *
)
��* +
;
��+ ,
var
�� 
dataType
�� 
=
�� !
GenerateDataTypeSql
�� %
(
��% &
column
��& ,
.
��, -
Type
��- 1
,
��1 2
column
��3 9
.
��9 :
Length
��: @
)
��@ A
;
��A B
if
�� 
(
�� 
	isPrimary
�� 
||
�� 
column
�� 
.
�� 
Required
�� #
)
��# $
{
�� 
dataType
�� 
=
�� 
$"
�� 
{
�� 
dataType
�� 
}
�� 
	 NOT NULL
�� $
"
��$ %
;
��% &
}
�� 
return
�� 	
$"
��
 
{
�� 
Escape
�� 
}
�� 
{
�� 

columnName
�� 
}
��  
{
��  !
Escape
��! '
}
��' (
{
��) *
dataType
��* 2
}
��2 3
"
��3 4
;
��4 5
}
�� 
	protected
�� 
virtual
�� 
string
�� !
GenerateDataTypeSql
�� .
(
��. /
string
��/ 5
type
��6 :
,
��: ;
int
��< ?
length
��@ F
)
��F G
{
�� 
string
�� 	
dataType
��
 
;
�� 
switch
�� 	
(
��
 
type
�� 
)
�� 
{
�� 
case
�� 
BoolType
��	 
:
�� 
{
�� 
dataType
�� 
=
�� 
$str
�� 
;
�� 
break
�� 

;
��
 
}
�� 
case
�� 
DateTimeType
��	 
:
�� 
case
��  
DateTimeOffsetType
��	 
:
�� 
{
�� 
dataType
�� 
=
�� 
$str
�� 5
;
��5 6
break
�� 

;
��
 
}
�� 
case
�� 
DecimalType
��	 
:
�� 
{
�� 
dataType
�� 
=
�� 
$str
�� 
;
��  
break
�� 

;
��
 
}
�� 
case
�� 

DoubleType
��	 
:
�� 
{
�� 
dataType
�� 
=
�� 
$str
�� 
;
�� 
break
�� 

;
��
 
}
�� 
case
�� 
	FloatType
��	 
:
�� 
{
�� 
dataType
�� 
=
�� 
$str
�� 
;
�� 
break
�� 

;
��
 
}
�� 
case
�� 
IntType
��	 
:
�� 
{
�� 
dataType
�� 
=
�� 
$str
�� 
;
�� 
break
�� 

;
��
 
}
�� 
case
�� 
LongType
��	 
:
�� 
{
�� 
dataType
�� 
=
�� 
$str
�� 
;
�� 
break
�� 

;
��
 
}
�� 
case
�� 
ByteType
��	 
:
�� 
{
�� 
dataType
�� 
=
�� 
$str
�� 
;
�� 
break
�� 

;
��
 
}
�� 
default
�� 
:
�� 
{
�� 
dataType
�� 
=
�� 
length
�� 
<=
�� 
$num
�� 
||
�� 
length
�� %
>
��& '
$num
��( ,
?
��- .
$str
��/ 9
:
��: ;
$"
��< >
VARCHAR(
��> F
{
��F G
length
��G M
}
��M N
)
��N O
"
��O P
;
��P Q
break
�� 

;
��
 
}
�� 
}
�� 
return
�� 	
dataType
��
 
;
�� 
}
�� 
}
�� 
}�� �{
s/Users/rokt/Documents/Rokt/Security-Engineer/Projects/DotnetSpider/src/DotnetSpider.MySql/MySqlFileEntityStorage.cs
	namespace 	
DotnetSpider
 
. 
MySql 
{ 
public 
class 
MySqlFileOptions 
{ 
private 	
readonly
 
IConfiguration !
_configuration" 0
;0 1
public 
MySqlFileOptions	 
( 
IConfiguration (
configuration) 6
)6 7
{ 
_configuration 
= 
configuration !
;! "
} 
public 
MySqlFileType	 
Type 
=> 
string %
.% &
IsNullOrWhiteSpace& 8
(8 9
_configuration9 G
[G H
$strH X
]X Y
)Y Z
? 
MySqlFileType 
. 
LoadFile 
: 
( 
MySqlFileType 
) 
Enum 
. 
Parse 
( 
typeof %
(% &
MySqlFileType& 3
)3 4
,4 5
_configuration6 D
[D E
$strE U
]U V
)V W
;W X
public 
bool	 

IgnoreCase 
=> 
string "
." #
IsNullOrWhiteSpace# 5
(5 6
_configuration6 D
[D E
$strE [
][ \
)\ ]
||^ `
bool  
.  !
Parse! &
(& '
_configuration' 5
[5 6
$str6 L
]L M
)M N
;N O
} 
public&& 
class&& "
MySqlFileEntityStorage&& $
:&&% &!
EntityFileStorageBase&&' <
{'' 
private(( 	
readonly((
  
ConcurrentDictionary(( '
<((' (
string((( .
,((. /
StreamWriter((0 <
>((< =
_streamWriters((> L
=((M N
new))  
ConcurrentDictionary)) 
<)) 
string)) "
,))" #
StreamWriter))$ 0
>))0 1
())1 2
)))2 3
;))3 4
public.. 
bool..	 

IgnoreCase.. 
{.. 
get.. 
;.. 
set..  #
;..# $
}..% &
=..' (
true..) -
;..- .
public00 
MySqlFileType00	 
MySqlFileType00 $
{00% &
get00' *
;00* +
set00, /
;00/ 0
}001 2
public77 
static77	 "
MySqlFileEntityStorage77 &
CreateFromOptions77' 8
(778 9
IConfiguration779 G
configuration77H U
)77U V
{88 
var99 
options99 
=99 
new99 
MySqlFileOptions99 %
(99% &
configuration99& 3
)993 4
;994 5
return:: 	
new::
 "
MySqlFileEntityStorage:: $
(::$ %
options::% ,
.::, -
Type::- 1
,::1 2
options::3 :
.::: ;

IgnoreCase::; E
)::E F
;::F G
};; 
publicBB "
MySqlFileEntityStorageBB	 
(BB  
MySqlFileTypeBB  -
fileTypeBB. 6
=BB7 8
MySqlFileTypeBB9 F
.BBF G
LoadFileBBG O
,BBO P
boolBBQ U

ignoreCaseBBV `
=BBa b
falseBBc h
)BBh i
{CC 
MySqlFileTypeDD 
=DD 
fileTypeDD 
;DD 

IgnoreCaseEE 
=EE 

ignoreCaseEE 
;EE 
}FF 
	protectedHH 
overrideHH 
asyncHH 
TaskHH 
StorageAsyncHH  ,
(HH, -
DataFlowContextHH- <
contextHH= D
,HHD E
TableMetadataHHF S
tableMetadataHHT a
,HHa b
IListHHc h
dataHHi m
)HHm n
{II 
varJJ 
writerJJ 
=JJ 
_streamWritersJJ 
.JJ 
GetOrAddJJ '
(JJ' (
tableMetadataJJ( 5
.JJ5 6
TypeNameJJ6 >
,JJ> ?
sKK 
=>KK 
	OpenWriteKK	 
(KK 
contextKK 
,KK 
tableMetadataKK )
,KK) *
$strKK+ 0
)KK0 1
)KK1 2
;KK2 3
switchMM 	
(MM
 
MySqlFileTypeMM 
)MM 
{NN 
caseOO 
MySqlFileTypeOO	 
.OO 
LoadFileOO 
:OO  
{PP 
awaitQQ 

WriteLoadFileAsyncQQ 
(QQ 
writerQQ $
,QQ$ %
tableMetadataQQ& 3
,QQ3 4
dataQQ5 9
)QQ9 :
;QQ: ;
breakRR 

;RR
 
}SS 
caseTT 
MySqlFileTypeTT	 
.TT 
	InsertSqlTT  
:TT  !
{UU 
awaitVV 

WriteInsertFileVV 
(VV 
writerVV !
,VV! "
tableMetadataVV# 0
,VV0 1
dataVV2 6
)VV6 7
;VV7 8
breakWW 

;WW
 
}XX 
}YY 
}ZZ 
public\\ 
override\\	 
void\\ 
Dispose\\ 
(\\ 
)\\  
{]] 
foreach^^ 

(^^ 
var^^ 
streamWriter^^ 
in^^ 
_streamWriters^^  .
)^^. /
{__ 
streamWriter`` 
.`` 
Value`` 
.`` 
Dispose`` 
(`` 
)``  
;``  !
}aa 
basecc 
.cc 
Disposecc 
(cc 
)cc 
;cc 
}dd 
privateff 	
asyncff
 
Taskff 
WriteLoadFileAsyncff '
(ff' (
StreamWriterff( 4
writerff5 ;
,ff; <
TableMetadataff= J
tableMetadataffK X
,ffX Y
IListffZ _
itemsff` e
)ffe f
{gg 
varhh 
builderhh 
=hh 
newhh 
StringBuilderhh "
(hh" #
)hh# $
;hh$ %
varii 
columnsii 
=ii 
tableMetadataii 
.ii 
Columnsii &
;ii& '
varjj "
isAutoIncrementPrimaryjj 
=jj 
tableMetadatajj  -
.jj- ."
IsAutoIncrementPrimaryjj. D
;jjD E
varll 
insertColumnsll 
=ll 
(mm "
isAutoIncrementPrimarymm 
?mm 
columnsmm %
.mm% &
Wheremm& +
(mm+ ,
c1mm, .
=>mm/ 1
c1mm2 4
.mm4 5
Keymm5 8
!=mm9 ;
tableMetadatamm< I
.mmI J
PrimarymmJ Q
.mmQ R
FirstmmR W
(mmW X
)mmX Y
)mmY Z
:mm[ \
columnsmm] d
)mmd e
.nn 
ToArraynn 
(nn 
)nn 
;nn 
foreachoo 

(oo 
varoo 
itemoo 
inoo 
itemsoo 
)oo 
{pp 
builderqq 
.qq 
Appendqq 
(qq 
$strqq 
)qq 
;qq 
foreachrr 
(rr 
varrr 
columnrr 
inrr 
insertColumnsrr (
)rr( )
{ss 
vartt 
valuett	 
=tt 
columntt 
.tt 
Valuett 
.tt 
PropertyInfott *
.tt* +
GetValuett+ 3
(tt3 4
itemtt4 8
)tt8 9
;tt9 :
valueuu 

=uu 
valueuu 
==uu 
nulluu 
?uu 
$struu 
:uu  !
MySqlHelperuu" -
.uu- .
EscapeStringuu. :
(uu: ;
valueuu; @
.uu@ A
ToStringuuA I
(uuI J
)uuJ K
)uuK L
;uuL M
buildervv 
.vv 
Appendvv 
(vv 
$strvv 
)vv 
.vv 
Appendvv 
(vv  
valuevv  %
)vv% &
.vv& '
Appendvv' -
(vv- .
$strvv. 1
)vv1 2
.vv2 3
Appendvv3 9
(vv9 :
$strvv: =
)vv= >
;vv> ?
}ww 
}xx 
awaitzz 
writerzz	 
.zz 
WriteLineAsynczz 
(zz 
builderzz &
.zz& '
ToStringzz' /
(zz/ 0
)zz0 1
)zz1 2
;zz2 3
}{{ 
private}} 	
async}}
 
Task}} 
WriteInsertFile}} $
(}}$ %
StreamWriter}}% 1
writer}}2 8
,}}8 9
TableMetadata}}: G
tableMetadata}}H U
,}}U V
IList}}W \
items}}] b
)}}b c
{~~ 
var 
builder 
= 
new 
StringBuilder "
(" #
)# $
;$ %
var
�� 
columns
�� 
=
�� 
tableMetadata
�� 
.
�� 
Columns
�� &
;
��& '
var
�� $
isAutoIncrementPrimary
�� 
=
�� 
tableMetadata
��  -
.
��- .$
IsAutoIncrementPrimary
��. D
;
��D E
var
�� 
tableSql
�� 
=
�� 
GenerateTableSql
�� "
(
��" #
tableMetadata
��# 0
)
��0 1
;
��1 2
var
�� 
insertColumns
�� 
=
�� 
(
�� $
isAutoIncrementPrimary
�� 
?
�� 
columns
�� %
.
��% &
Where
��& +
(
��+ ,
c1
��, .
=>
��/ 1
c1
��2 4
.
��4 5
Key
��5 8
!=
��9 ;
tableMetadata
��< I
.
��I J
Primary
��J Q
.
��Q R
First
��R W
(
��W X
)
��X Y
)
��Y Z
:
��[ \
columns
��] d
)
��d e
.
�� 
ToArray
�� 
(
�� 
)
�� 
;
�� 
foreach
�� 

(
�� 
var
�� 
item
�� 
in
�� 
items
�� 
)
�� 
{
�� 
builder
�� 
.
�� 
Append
�� 
(
�� 
$"
�� !
INSERT IGNORE INTO 
�� (
{
��( )
tableSql
��) 1
}
��1 2
 (
��2 4
"
��4 5
)
��5 6
;
��6 7
var
�� 

lastColumn
�� 
=
�� 
insertColumns
�� "
.
��" #
Last
��# '
(
��' (
)
��( )
;
��) *
foreach
�� 
(
�� 
var
�� 
column
�� 
in
�� 
insertColumns
�� (
)
��( )
{
�� 
builder
�� 
.
�� 
Append
�� 
(
�� 
column
�� 
.
�� 
Equals
�� !
(
��! "

lastColumn
��" ,
)
��, -
?
��. /
$"
��0 2
`
��2 3
{
��3 4
column
��4 :
.
��: ;
Key
��; >
}
��> ?
`
��? @
"
��@ A
:
��B C
$"
��D F
`
��F G
{
��G H
column
��H N
.
��N O
Key
��O R
}
��R S
`, 
��S V
"
��V W
)
��W X
;
��X Y
}
�� 
builder
�� 
.
�� 
Append
�� 
(
�� 
$str
�� 
)
��  
;
��  !
foreach
�� 
(
�� 
var
�� 
column
�� 
in
�� 
insertColumns
�� (
)
��( )
{
�� 
var
�� 
value
��	 
=
�� 
column
�� 
.
�� 
Value
�� 
.
�� 
PropertyInfo
�� *
.
��* +
GetValue
��+ 3
(
��3 4
item
��4 8
)
��8 9
;
��9 :
value
�� 

=
�� 
value
�� 
==
�� 
null
�� 
?
�� 
$str
�� 
:
��  !
MySqlHelper
��" -
.
��- .
EscapeString
��. :
(
��: ;
value
��; @
.
��@ A
ToString
��A I
(
��I J
)
��J K
)
��K L
;
��L M
builder
�� 
.
�� 
Append
�� 
(
�� 
column
�� 
.
�� 
Equals
�� !
(
��! "

lastColumn
��" ,
)
��, -
?
��. /
$"
��0 2
'
��2 3
{
��3 4
value
��4 9
}
��9 :
'
��: ;
"
��; <
:
��= >
$"
��? A
'
��A B
{
��B C
value
��C H
}
��H I
', 
��I L
"
��L M
)
��M N
;
��N O
}
�� 
builder
�� 
.
�� 
Append
�� 
(
�� 
$"
�� 
);
�� 
{
�� 
Environment
�� #
.
��# $
NewLine
��$ +
}
��+ ,
"
��, -
)
��- .
;
��. /
}
�� 
await
�� 
writer
��	 
.
�� 
WriteLineAsync
�� 
(
�� 
builder
�� &
.
��& '
ToString
��' /
(
��/ 0
)
��0 1
)
��1 2
;
��2 3
builder
�� 

.
��
 
Clear
�� 
(
�� 
)
�� 
;
�� 
}
�� 
	protected
�� 
virtual
�� 
string
�� 
GenerateTableSql
�� +
(
��+ ,
TableMetadata
��, 9
tableMetadata
��: G
)
��G H
{
�� 
var
�� 
	tableName
�� 
=
�� 

GetNameSql
�� 
(
�� 
tableMetadata
�� +
.
��+ ,
Schema
��, 2
.
��2 3
Table
��3 8
)
��8 9
;
��9 :
var
�� 
database
�� 
=
�� 

GetNameSql
�� 
(
�� 
tableMetadata
�� *
.
��* +
Schema
��+ 1
.
��1 2
Database
��2 :
)
��: ;
;
��; <
return
�� 	
string
��
 
.
��  
IsNullOrWhiteSpace
�� #
(
��# $
database
��$ ,
)
��, -
?
��. /
$"
��0 2
`
��2 3
{
��3 4
	tableName
��4 =
}
��= >
`
��> ?
"
��? @
:
��A B
$"
��C E
`
��E F
{
��F G
database
��G O
}
��O P
`.`
��P S
{
��S T
	tableName
��T ]
}
��] ^
`
��^ _
"
��_ `
;
��` a
}
�� 
private
�� 	
string
��
 

GetNameSql
�� 
(
�� 
string
�� "
name
��# '
)
��' (
{
�� 
if
�� 
(
�� 
string
�� 
.
��  
IsNullOrWhiteSpace
��  
(
��  !
name
��! %
)
��% &
)
��& '
{
�� 
return
�� 

null
�� 
;
�� 
}
�� 
return
�� 	

IgnoreCase
��
 
?
�� 
name
�� 
.
�� 
ToLowerInvariant
�� ,
(
��, -
)
��- .
:
��/ 0
name
��1 5
;
��5 6
}
�� 
}
�� 
}�� 